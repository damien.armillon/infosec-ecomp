open Batteries
open Rtl
open Linear
open Prog
open Utils
open Report
open Linear_print
open Options
open Linear_liveness

let succs_of_rtl_instr (i: rtl_instr) =
  match i with
  | Rtl.Rbranch (_, _, _, s1) -> [s1]
  | Rtl.Rjmp s -> [s]
  | _ -> []

let rec succs_of_rtl_instrs il : int list =
  List.concat (List.map succs_of_rtl_instr il)

(* effectue un tri topologique des blocs.  *)
let sort_blocks (nodes: (int, rtl_instr list) Hashtbl.t) entry =
  let rec add_block order n =
   if not (List.mem n order) then
     let new_order = order @ [n] in
     let new_block = Hashtbl.find nodes n in
     let successors = List.filter_map (
       fun instr -> match instr with
       | Rjmp(s) -> Some s
       | Rbranch(_,_,_,s) -> Some s
       | _ -> None
       ) new_block in
      let a = List.fold_left (fun acc s -> add_block acc s) new_order successors in
      a else
     order
  in
  add_block [] entry


(* Supprime les jumps inutiles (Jmp à un label défini juste en dessous). *)
let rec remove_useless_jumps (l: rtl_instr list) =
   match l with
   | [] -> []
   | Rjmp(s)::Rlabel(s')::r -> 
    if s = s' then Rlabel(s')::remove_useless_jumps r
    else Rjmp(s)::Rlabel(s')::remove_useless_jumps r
   | i::r -> i::remove_useless_jumps r 

(* Remove labels that are never jumped to. *)
let remove_useless_labels (l: rtl_instr list) =
   let label_to_jump = List.fold_left (fun acc instr ->
    match instr with
    | Rjmp(s) -> Set.add s acc
    | Rbranch(_,_,_,s) -> Set.add s acc
    | _ -> acc
    ) Set.empty l in
   List.filter (fun instr -> 
    match instr with
    | Rlabel(s) -> Set.mem s label_to_jump
    | _ -> true
    ) l

let linear_of_rtl_fun
    ({ rtlfunargs; rtlfunbody; rtlfunentry; rtlfuninfo; rtlfunstksz }: rtl_fun) =
  let block_order = sort_blocks rtlfunbody rtlfunentry in
  let linearinstrs =
    Rjmp rtlfunentry ::
    List.fold_left (fun l n ->
        match Hashtbl.find_option rtlfunbody n with
        | None -> l
        | Some li -> l @ Rlabel(n) :: li
      ) [] block_order in
  { linearfunargs = rtlfunargs;
    linearfunbody =
      linearinstrs |> remove_useless_jumps |> remove_useless_labels;
    linearfuninfo = rtlfuninfo;
    linearfunstksz = rtlfunstksz;
  }

let linear_of_rtl_gdef = function
    Gfun f -> Gfun (linear_of_rtl_fun f)

let linear_of_rtl r =
  assoc_map linear_of_rtl_gdef r

let pass_linearize rtl =
  let linear = linear_of_rtl rtl in
  let lives = liveness_linear_prog linear in
  dump !linear_dump (fun oc -> dump_linear_prog oc (Some lives)) linear
    (fun file () -> add_to_report "linear" "Linear" (Code (file_contents file)));
  OK (linear, lives)
