open Batteries
open Cfg
open Prog
open Utils

(* Analyse de vivacité *)

(* [vars_in_expr e] renvoie l'ensemble des variables qui apparaissent dans [e]. *)
let rec vars_in_expr (e: expr) =
   match e with
   | Evar(str) -> Set.singleton(str)
   | Estk i
   | Eint(i) -> Set.empty
   | Eunop(_,e1) -> vars_in_expr e1
   | Ebinop(_,e1,e2) -> Set.union (vars_in_expr e1) (vars_in_expr e2)
   | Ecall(_,el) -> List.fold_left (fun acc expr -> Set.union acc (vars_in_expr expr)) Set.empty el
   | Eload (e,n) -> vars_in_expr e

   (* [live_cfg_node node live_after] renvoie l'ensemble des variables vivantes
   avant un nœud [node], étant donné l'ensemble [live_after] des variables
   vivantes après ce nœud. *)
let live_cfg_node (node: cfg_node) (live_after: string Set.t) =
      match node with
      | Cnop(_) -> live_after
      | Cassign(str, e,_) -> Set.union (vars_in_expr e) (Set.diff live_after (Set.singleton str))
      | Creturn(e)
      | Cprint(e,_)
      | Ccmp(e,_,_) -> Set.union (vars_in_expr e) live_after
      | Ccall(_, el, _) -> List.fold_left (fun acc expr -> Set.union acc (vars_in_expr expr)) live_after el
      | Cstore(e1, e2,_,_) -> Set.union (Set.union (vars_in_expr e2) (vars_in_expr e1)) live_after


(* [live_after_node cfg n] renvoie l'ensemble des variables vivantes après le
   nœud [n] dans un CFG [cfg]. [lives] est l'état courant de l'analyse,
   c'est-à-dire une table dont les clés sont des identifiants de nœuds du CFG et
   les valeurs sont les ensembles de variables vivantes avant chaque nœud. *)
let live_after_node cfg n (lives: (int, string Set.t) Hashtbl.t) : string Set.t =
   Set.fold (fun succ acc->
      match Hashtbl.find_option lives succ with
      | None -> acc
      | Some(set) -> Set.union acc set
      ) (succs cfg n) Set.empty

(* [live_cfg_nodes cfg lives] effectue une itération du calcul de point fixe.

   Cette fonction met à jour l'état de l'analyse [lives] et renvoie un booléen
   qui indique si le calcul a progressé durant cette itération (i.e. s'il existe
   au moins un nœud n pour lequel l'ensemble des variables vivantes avant ce
   nœud a changé). *)
let live_cfg_nodes cfg (lives : (int, string Set.t) Hashtbl.t) =
   let reverse_order_nodes = List.sort (fun n1 n2 -> compare (fst n2) (fst n2) )(Hashtbl.to_list cfg) in
   List.fold_left (fun has_changed (n, node) ->
      let live_in_node = live_cfg_node node (live_after_node cfg n lives) in
      let old_live = Hashtbl.find_option lives n in
      Hashtbl.replace lives n live_in_node;
      match old_live with
      | None -> true
      | Some(old_live) ->
         if not (has_changed ||Set.equal live_in_node (old_live)) then true else has_changed
      ) false reverse_order_nodes

let rec live_cfg_nodes_rec cfg lives =
   if not (live_cfg_nodes cfg lives) then lives else live_cfg_nodes_rec cfg lives
(* [live_cfg_fun f] calcule l'ensemble des variables vivantes avant chaque nœud
   du CFG en itérant [live_cfg_nodes] jusqu'à ce qu'un point fixe soit atteint.
   *)
let live_cfg_fun (f: cfg_fun) : (int, string Set.t) Hashtbl.t =
  let lives = Hashtbl.create 17 in
  let cfg = f.cfgfunbody in
  live_cfg_nodes_rec cfg lives
