open Batteries
open Elang
open Cfg
open Utils
open Prog
open Report
open Cfg_print
open Options
open Elang_gen

(* [cfg_expr_of_eexpr struct_table fun_typ funvartyp funvarinmem e] converts an [Elang.expr] into a [expr res]. This should
   always succeed and be straightforward.

   In later versions of this compiler, you will add more things to [Elang.expr]
   but not to [Cfg.expr], hence the distinction.
*)
let rec cfg_expr_of_eexpr struct_table fun_typ (funvartyp: (string, typ) Hashtbl.t) (funvarinmem: (string, int) Hashtbl.t) (e: Elang.expr) : expr res =
  match e with
  | Elang.Ebinop (b, e1, e2) ->
      let* t1 = type_expr struct_table funvartyp fun_typ e1 in
      let* t2 = type_expr struct_table funvartyp fun_typ e2 in
      let* value1 = cfg_expr_of_eexpr struct_table fun_typ funvartyp funvarinmem e1 in
      let* value2 = cfg_expr_of_eexpr struct_table fun_typ funvartyp funvarinmem e2 in
      (match t1 with
      | Tptr t ->
         let* size = size_type t struct_table in
         OK(Ebinop(b, value1, Ebinop(Emul, value2, Eint size)))
      | _ -> 
         (match t2 with
         | Tptr t ->
            let* size = size_type t struct_table in
            OK(Ebinop(b, (Ebinop(Emul, value1, Eint size)), value2))
         | _ -> 
          OK (Ebinop (b, value1, value2))
         ))
    
  | Elang.Eunop (u, e) ->
    cfg_expr_of_eexpr struct_table fun_typ funvartyp funvarinmem e >>= fun ee ->
    OK (Eunop (u, ee))
  | Elang.Eint i -> OK (Eint i)
  | Elang.Echar c -> OK (Eint (Char.code c))
  | Elang.Evar v ->
    (
      match Hashtbl.find_option funvarinmem v with
      | Some offset ->
        let* mem_to_read = size_type (Hashtbl.find funvartyp v) struct_table in
        OK(Eload(Estk offset,mem_to_read))
      | None ->OK (Evar v)
    )
  | Elang.Ecall(name, elist) ->
    let* args_expr = list_map_res (cfg_expr_of_eexpr struct_table fun_typ funvartyp funvarinmem) elist in
    OK(Cfg.Ecall(name, args_expr))
  | Elang.Eaddrof (e) ->
    (match e with
    | Elang.Evar v ->
      (match Hashtbl.find_option funvarinmem v with
      | Some n -> OK(Estk n)
      | None -> Error "& on a variable not in memory (cfg gen)"
      )
    | _ -> Error "unauthorised operation: & on non var"
    )
  | Elang.Eload e ->
    let* cfg_e = cfg_expr_of_eexpr struct_table fun_typ funvartyp funvarinmem e in
    let* type_to_load = type_expr struct_table funvartyp fun_typ e in
    (match type_to_load with
    | Tptr t ->
       let* size = size_type t struct_table in
       OK(Eload(cfg_e,size))
    | _ -> Error "Can't load non pointer")
    | _ -> Error "souffle dans mon trou"

(* [cfg_node_of_einstr struct_table fun_typ funvartyp funvarinmem next cfg succ i] builds the CFG node(s) that correspond
   to the E instruction [i].

   [cfg] is the current state of the control-flow graph.

   [succ] is the successor of this node in the CFG, i.e. where to go after this
   instruction.

   [next] is the next available CFG node identifier.

   This function returns a pair (n, next) where [n] is the identifer of the
   node generated, and [next] is the new next available CFG node identifier.

   Hint: several nodes may be generated for a single E instruction.
*)
let rec cfg_node_of_einstr struct_table fun_typ funvartyp (funvarinmem: (string, int) Hashtbl.t) (next: int) (cfg : (int, cfg_node) Hashtbl.t)
    (succ: int) (i: instr) : (int * int) res =
  match i with
  | Elang.Iassign (v, e) ->
    let* value = cfg_expr_of_eexpr struct_table fun_typ funvartyp funvarinmem e in
      (match Hashtbl.find_option funvarinmem v with
      | Some addr ->
         let type_var = Hashtbl.find funvartyp v in
         let* size_write = size_type type_var struct_table in
         Hashtbl.replace cfg next (Cstore(Estk addr,value,size_write, succ));
         OK(next, next+1)
      | None ->
        let* e = cfg_expr_of_eexpr struct_table fun_typ funvartyp funvarinmem e in
        Hashtbl.replace cfg next (Cassign(v,e,succ));
        OK (next, next + 1))

  | Elang.Iif (c, ithen, ielse) ->
    cfg_expr_of_eexpr struct_table fun_typ funvartyp funvarinmem c >>= fun c ->
    cfg_node_of_einstr struct_table fun_typ funvartyp funvarinmem next cfg succ ithen >>= fun (nthen, next) ->
    cfg_node_of_einstr struct_table fun_typ funvartyp funvarinmem next cfg succ ielse  >>= fun (nelse, next) ->
    Hashtbl.replace cfg next (Ccmp(c, nthen, nelse)); OK (next, next + 1)
  | Elang.Iwhile (c, i) ->
    cfg_expr_of_eexpr struct_table fun_typ funvartyp funvarinmem c >>= fun c ->
    let (cmp, next) = (next, next + 1) in
    cfg_node_of_einstr struct_table fun_typ funvartyp funvarinmem next cfg cmp i >>= fun (nthen, next) ->
    Hashtbl.replace cfg cmp (Ccmp(c, nthen, succ)); OK (cmp, next + 1)
  | Elang.Iblock il ->
    List.fold_right (fun i acc ->
        acc >>= fun (succ, next) ->
        cfg_node_of_einstr struct_table fun_typ funvartyp funvarinmem next cfg succ i
      ) il (OK (succ, next))
  | Elang.Ireturn e ->
    cfg_expr_of_eexpr struct_table fun_typ funvartyp funvarinmem e >>= fun e ->
    Hashtbl.replace cfg next (Creturn e); OK (next, next + 1)
  | Elang.Iprint e ->
    cfg_expr_of_eexpr struct_table fun_typ funvartyp funvarinmem e >>= fun e ->
    Hashtbl.replace cfg next (Cprint (e,succ));
    OK (next, next + 1)
  | Elang.Icall(name, elist) ->
    let* args_expr = list_map_res (cfg_expr_of_eexpr struct_table fun_typ funvartyp funvarinmem) elist in
    Hashtbl.replace cfg next (Ccall(name, args_expr, succ));
    OK (next,next+1)
  | Elang.Istore (e1, e2) ->
    let* addr = cfg_expr_of_eexpr struct_table fun_typ funvartyp funvarinmem e1 in
    let* value = cfg_expr_of_eexpr struct_table fun_typ funvartyp funvarinmem e2 in
    let* t = type_expr struct_table funvartyp fun_typ e1 in
    let* size_write = size_type t struct_table in
    Hashtbl.replace cfg next (Cstore(addr,value,size_write, succ));
    OK(next, next+1)
  | _ -> Error "soufle dans mon trou"




(* Some nodes may be unreachable after the CFG is entirely generated. The
   [reachable_nodes n cfg] constructs the set of node identifiers that are
   reachable from the entry node [n]. *)
let rec reachable_nodes n (cfg: (int,cfg_node) Hashtbl.t) =
  let rec reachable_aux n reach =
    if Set.mem n reach then reach
    else let reach = Set.add n reach in
      match Hashtbl.find_option cfg n with
      | None -> reach
      | Some (Cnop succ)
      | Some (Cprint (_, succ))
      | Some (Ccall(_,_,succ))
      | Some (Cstore(_,_,_,succ))
      | Some (Cassign (_, _, succ)) -> reachable_aux succ reach
      | Some (Creturn _) -> reach
      | Some (Ccmp (_, s1, s2)) ->
        reachable_aux s1 (reachable_aux s2 reach)
  in reachable_aux n Set.empty

(* [cfg_fun_of_efun f] builds the CFG for E function [f]. *)
let cfg_fun_of_efun struct_table fun_typ { funargs; funbody; funvartyp; funvarinmem; funstksz } =
  let cfg = Hashtbl.create 17 in
  Hashtbl.replace cfg 0 (Creturn (Eint 0));
  cfg_node_of_einstr struct_table fun_typ funvartyp funvarinmem 1 cfg 0 funbody >>= fun (node, _) ->
  (* remove unreachable nodes *)
  let r = reachable_nodes node cfg in
  Hashtbl.filteri_inplace (fun k _ -> Set.mem k r) cfg;
  OK { cfgfunargs = fst (List.split funargs);
       cfgfunbody = cfg;
       cfgentry = node;
       cfgfunstksz = funstksz;
     }

let cfg_gdef_of_edef struct_table fun_typ gd =
  match gd with
    Gfun f -> cfg_fun_of_efun struct_table fun_typ f >>= fun f -> OK (Gfun f)

let cfg_prog_of_eprog (ep: eprog) : cfg_fun prog res =
  let (fun_list, struct_table) = ep in
  let fun_typ = Hashtbl.create (3+(List.length fun_list)) in
  List.iter (fun (name,pro) ->
   match pro with
   | Gfun f -> Hashtbl.replace fun_typ name ((snd (List.split f.funargs)),f.funrettype);
   )fun_list;
  Hashtbl.replace fun_typ "print" ([Tint], Tvoid);
  Hashtbl.replace fun_typ "print_int" ([Tint], Tvoid);
  Hashtbl.replace fun_typ "print_char" ([Tchar], Tvoid);
  assoc_map_res (fun fname -> cfg_gdef_of_edef struct_table fun_typ) fun_list

let pass_cfg_gen ep =
  match cfg_prog_of_eprog ep with
  | Error msg ->
    record_compile_result ~error:(Some msg) "CFG"; Error msg
  | OK cfg ->
    record_compile_result "CFG";
    dump !cfg_dump dump_cfg_prog cfg (call_dot "cfg" "CFG");
    OK cfg
