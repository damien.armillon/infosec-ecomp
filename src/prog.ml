open Batteries
open Utils

type mem_access_size =
  | MAS1
  | MAS4
  | MAS8

let string_of_mem_access_size mas =
  match mas with
  | MAS1 -> "{1}"
  | MAS4 -> "{4}"
  | MAS8 -> "{8}"

let mas_of_size n =
  match n with
  | 1 -> OK MAS1
  | 4 -> OK MAS4
  | 8 -> OK MAS8
  | _ -> Error (Printf.sprintf "Unknown memory access size for size = %d" n)


let size_of_mas mas =
  match mas with
  | MAS1 -> 1
  | MAS4 -> 4
  | MAS8 -> 8

let archi_mas () =
  match !Archi.archi with
  | A64 -> MAS8
  | A32 -> MAS4


type 'a gdef = Gfun of 'a

type 'a prog = (string * 'a gdef) list


let dump_gdef dump_fun oc gd =
  match gd with
  | (fname, Gfun f) ->
    dump_fun oc fname f;
    Format.fprintf oc "\n"

let dump_prog dump_fun oc =
  List.iter (dump_gdef dump_fun oc)

type 'a state = {
  env: (string, 'a) Hashtbl.t;
  mem: Mem.t
}

let init_state memsize =
  {
    mem = Mem.init memsize;
    env = Hashtbl.create 17;
  }

let set_val env v i =
  Hashtbl.replace env v i

let get_val env v =
  Hashtbl.find_option env v

let find_function (ep: 'a prog) fname : 'a res =
  match List.assoc_opt fname ep with
  | Some (Gfun f) -> OK f
  | _ -> Error (Format.sprintf "Unknown function %s\n" fname)

type typ =
    Tint
  | Tchar
  | Tvoid
  | Tptr of typ
  | Tstruct of string
  
let rec string_of_typ t =
  match t with
  | Tint -> "int"
  | Tchar -> "char"
  | Tvoid -> "void"
  | Tptr ptt-> Printf.sprintf "%s*" (string_of_typ ptt)
  | Tstruct str -> Printf.sprintf "struct %s" str

let rec size_type (t: typ) (struct_table: (string, (string * typ) list) Hashtbl.t):  int res =
  match t with
  | Tint -> OK (Archi.wordsize () )
  | Tptr _ -> OK (Archi.nbits () /8)
  | Tchar -> OK 1
  | Tvoid -> Error "Void can't be stored in memory"
  | Tstruct str ->
    match Hashtbl.find_option struct_table str with
    | None -> Error (Printf.sprintf "struct not found: %s in %s" str (string_of_string_set (Set.of_list (fst (List.split (Hashtbl.to_list struct_table))))))
    | Some contentl -> List.fold_left (fun acc (_,t) ->
      let* acc = acc in
      let* s = size_type t struct_table in
      OK (acc + s)
    ) (OK 0) contentl

let rec field_offset
  (structs: (string, (string * typ) list) Hashtbl.t)
  (s: string) (f: string) : int res =
  match Hashtbl.find_option structs s with
  | Some l ->
    let* (offset,found) = List.fold_left (fun acc (field, fieldtyp) ->
        let* (offset, found) = acc in
        if found then OK(offset, found)
        else (
          Printf.printf "f: %s field: %s %b\n" f field (field = f);
          if field = f then OK(offset, true)
          else
            let* size = size_type fieldtyp structs in
            OK(offset + size, false))
      ) (OK (0,false)) l in
    if not found
      then Error (Printf.sprintf "Field %s not found in %s (%s)" f s (string_of_string_set (Set.of_list (fst(List.split l)))))
    else OK offset

  | None -> Error (Printf.sprintf "struct not found there: %s" s)

let rec field_type
  (structs: (string, (string * typ) list) Hashtbl.t)
  (s: string) (f: string) : typ res =
  match Hashtbl.find_option structs s with
  | Some l ->
    let ft = List.assoc_opt f l in
    (match ft with
    | Some t -> OK t
    | None -> Error (Printf.sprintf "Field %s not found in %s (%s)" f s (string_of_string_set (Set.of_list (fst(List.split l))))))

  | None -> Error (Printf.sprintf "struct not found: %s" s)