open Batteries
open Cfg
open Elang_run
open Cfg_run
open Prog
open Utils
open Report
open Cfg_print
open Options

(* [simple_eval_eexpr e] evaluates an expression [e] with no variables. Raises
   an exception if the expression contains variables. *)
let rec simple_eval_eexpr (e: expr) : int =
  match e with
  | Ebinop(b, e1, e2) ->
    let v1 = simple_eval_eexpr e1 in
    let v2 = simple_eval_eexpr e2 in
    let v = eval_binop b v1 v2 in
    v
  | Eunop(u, e) ->
    let v1 = simple_eval_eexpr e in
    let v = (eval_unop u v1) in
    v
  | Estk i
  | Eint i -> i
  | Ecall (name,_) -> failwith (Printf.sprintf "tried to call %s" name)
  | Eload (e, _) -> failwith "No load authorized"
  | _ -> failwith "unevaluable expr (const propagation)"


(* If an expression contains variables, we cannot simply evaluate it. *)

(* [has_vars e] indicates whether [e] contains variables. *)
let rec has_vars (e: expr) =
   match e with
   | Estk n -> true (*TODO set to false later*)
   | Eint(n) -> false
   | Evar(str) -> true
   | Eload(e',_)
   | Eunop(_,e') -> has_vars e'
   | Ebinop(_,e',e'') -> has_vars e' || has_vars e''
   | Ecall(_,el) -> true

let const_prop_binop b e1 e2 =
  let e = Ebinop (b, e1, e2) in
  if has_vars e
  then e
  else Eint (simple_eval_eexpr e)

let const_prop_unop u e =
  let e = Eunop (u, e) in
  if has_vars e
  then e
  else Eint (simple_eval_eexpr e)


let rec const_prop_expr (e: expr) =
   if has_vars e
    then e
  else match e with
    | Ebinop(_,_,_)
    | Eunop(_,_)
    | Eint(_) -> Eint (simple_eval_eexpr e)
    | Ecall(name,el) -> Ecall(name, List.map const_prop_expr el)
    | _ -> e

let constant_propagation_instr (i: cfg_node) : cfg_node =
    match i with
    | Cassign(str,e,n) -> Cassign(str, const_prop_expr e, n)
    | Creturn(e) -> Creturn(const_prop_expr e)
    | Cprint(e, n) -> Cprint(const_prop_expr e, n)
    | Ccmp(e,n,n') -> Ccmp(const_prop_expr e, n, n')
    | Cnop(n) -> Cnop(n)
    | Ccall(str, el, n) -> Ccall(str, List.map const_prop_expr el, n)
    | Cstore(e1,e2, n, s) -> Cstore(e1, const_prop_expr e2, n, s)

let constant_propagation_fun ({ cfgfunargs; cfgfunbody; cfgentry } as f: cfg_fun) =
  let ht = Hashtbl.map (fun n m ->
      constant_propagation_instr m
    ) cfgfunbody in
  { f with cfgfunbody = ht}

let constant_propagation_gdef = function
    Gfun f ->
    Gfun (constant_propagation_fun f)

let constant_propagation p =
  if !Options.no_cfg_constprop
  then p
  else assoc_map constant_propagation_gdef p

let pass_constant_propagation p =
  let cfg = constant_propagation p in
  record_compile_result "Constprop";
  dump (!cfg_dump >*> fun s -> s ^ "1") dump_cfg_prog cfg
    (call_dot "cfg-after-cstprop" "CFG after Constant Propagation");
  OK cfg
