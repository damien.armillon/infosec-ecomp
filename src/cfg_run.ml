open Prog
open Elang
open Elang_run
open Batteries
open BatList
open Cfg
open Utils
open Builtins

let rec eval_cfgexpr sp oc cp st (e: expr) : (int * int Prog.state) res =
  match e with
  | Ebinop(b, e1, e2) ->
    let* (v1, st) = eval_cfgexpr sp oc cp st e1 in
    let* (v2, st) = eval_cfgexpr sp oc cp st e2 in
    let v = eval_binop b v1 v2 in
    OK (v, st)
  | Eunop(u, e) ->
    let* (v1,st) = eval_cfgexpr sp oc cp st e in
    let v = (eval_unop u v1) in
    OK (v, st)
  | Eint i -> OK (i, st)
  | Evar s ->
    begin match Hashtbl.find_option st.env s with
      | Some v -> OK (v, st)
      | None -> Error (Printf.sprintf "Unknown variable %s\n" s)
    end
  | Ecall(name,el) ->
    let* (params, st) = List.fold_left (fun acc expr ->
      let* (expr_list,st) = acc in
      let* (new_expr, st) = eval_cfgexpr sp oc cp st expr in
      OK(expr_list@[new_expr], st)
      ) (OK([],st)) el in
    (match find_function cp name with
    | OK f ->
      let* (i,st) = eval_cfgfun (sp + f.cfgfunstksz) cp oc st name f params in
      (match i with
      | None -> Error (Printf.sprintf "Error while runing %s" name)
      | Some(i) -> OK(i,st))
    | Error _ ->
      let* ret = do_builtin oc st.mem name params in
      (match ret with
      | Some n -> OK(n, st)
      | None -> OK(0, st)))
  | Estk i ->Printf.printf "Addres %d + %d\n" i sp; OK(i+sp, st)
  | Eload (e,size) ->
    let* (addr,st) = eval_cfgexpr sp oc cp st e in
    Printf.printf "On lit à %d\n" addr;
    let* value = Mem.read_bytes_as_int st.mem addr size in
    OK(value, st)


and eval_cfginstr sp cp oc st ht (n: int): (int * int state) res =
  match Hashtbl.find_option ht n with
  | None -> Error (Printf.sprintf "Invalid node identifier\n")
  | Some node ->
    match node with
    | Cnop succ ->
      eval_cfginstr sp cp oc st ht succ
    | Cassign(v, e, succ) ->
      let* (i, st) = eval_cfgexpr sp oc cp st e in
      Hashtbl.replace st.env v i;
      eval_cfginstr sp cp oc st ht succ
    | Ccmp(cond, i1, i2) ->
      let* (i,st) = eval_cfgexpr sp oc cp st cond in
      if i = 0 then eval_cfginstr sp cp oc st ht i2 else eval_cfginstr sp cp oc st ht i1
    | Creturn(e) ->
      let* (e,st) = eval_cfgexpr sp oc cp st e in
      OK (e, st)
    | Cprint(e, succ) ->
      let* (e, st) = eval_cfgexpr sp oc cp st e in
      Format.fprintf oc "%d\n" e;
      eval_cfginstr sp cp oc st ht succ

    | Ccall(name, el, succ) ->
      let* (params, st) = List.fold_left (fun acc expr ->
        let* (expr_list,st) = acc in
        let* (new_expr, st) = eval_cfgexpr sp oc cp st expr in
        OK(expr_list@[new_expr], st)
        ) (OK([],st)) el in
      (match find_function cp name with
      | OK f ->
        let* (i,st) = eval_cfgfun (sp + f.cfgfunstksz) cp oc st name f params in
        (match i with
        | None -> Error (Printf.sprintf "Error while runing %s" name)
        | Some(i) -> eval_cfginstr sp cp oc st ht succ)
      | Error _ ->
        (match find_function cp name with
        | OK f -> 
          let* (res,st) = eval_cfgfun (sp+f.cfgfunstksz) cp oc st name f params in
          (match res with
          | Some(n) -> OK(n, st)
          | None -> Error (Format.sprintf "Error with function %s" name))
        | Error _ ->
          let* ret = do_builtin oc st.mem name params in
          (match ret with
          | Some n -> OK(n, st)
          | None -> eval_cfginstr sp cp oc st ht succ)))
  | Cstore (e1,e2,size,succ) ->
    let* (addr,st) = eval_cfgexpr sp oc cp st e1 in
    let* (value,st) = eval_cfgexpr sp oc cp st e2 in
    Printf.printf "We write %d in %d\n" value addr;
    let bytes_to_write = split_bytes (size) value in
    let* _ = Mem.write_bytes st.mem addr bytes_to_write in
    eval_cfginstr sp cp oc st ht succ


  


and eval_cfgfun sp cp oc st cfgfunname { cfgfunargs;
                                      cfgfunbody;
                                      cfgentry} vargs =
  let st' = { st with env = Hashtbl.create 17 } in
  match List.iter2 (fun a v -> Hashtbl.replace st'.env a v) cfgfunargs vargs with
  | () -> eval_cfginstr sp cp oc st' cfgfunbody cfgentry >>= fun (v, st') ->
    OK (Some v, {st' with env = st.env})
  | exception Invalid_argument _ ->
    Error (Format.sprintf "CFG: Called function %s with %d arguments, expected %d.\n"
             cfgfunname (List.length vargs) (List.length cfgfunargs)
          )

let eval_cfgprog oc cp memsize params =
  let st = init_state memsize in
  find_function cp "main" >>= fun f ->
  let n = List.length f.cfgfunargs in
  let params = take n params in
  eval_cfgfun 0 cp oc st "main" f params >>= fun (v, st) ->
  OK v


