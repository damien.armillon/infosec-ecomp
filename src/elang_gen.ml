open Ast
open Elang
open Prog
open Report
open Options
open Batteries
open Elang_print
open Utils

let tag_is_binop =
  function
    Tadd -> true
  | Tsub -> true
  | Tmul -> true
  | Tdiv -> true
  | Tmod -> true
  | Txor -> true
  | Tcle -> true
  | Tclt -> true
  | Tcge -> true
  | Tcgt -> true
  | Tceq -> true
  | Tne  -> true
  | _    -> false

let binop_of_tag =
  function
    Tadd -> Eadd
  | Tsub -> Esub
  | Tmul -> Emul
  | Tdiv -> Ediv
  | Tmod -> Emod
  | Txor -> Exor
  | Tcle -> Ecle
  | Tclt -> Eclt
  | Tcge -> Ecge
  | Tcgt -> Ecgt
  | Tceq -> Eceq
  | Tne -> Ecne
  | _ -> assert false

  let tag_is_unop =
    function
    | Tneg -> true
    | _    -> false
  
  let unop_of_tag =
    function
    | Tneg -> Eneg
    | _ -> assert false
  

let rec type_expr struct_table
  (typ_var : (string,typ) Hashtbl.t)
  (typ_fun : (string, typ list * typ) Hashtbl.t)
  (e: expr)
  : typ res =
  match e with
  | Ebinop(op,e1,e2) -> type_expr struct_table typ_var typ_fun e1
  | Eunop(op,e) -> type_expr struct_table typ_var typ_fun e
  | Eint n -> OK Tint
  | Echar c -> OK Tchar
  | Evar v ->
    (match Hashtbl.find_option typ_var v with
    | Some t ->(
      match t with
      | Tstruct name -> OK (Tptr t)
      | _ -> OK t
    )
    | None -> Error (Format.sprintf "variable %s not found (unknown type)" v)
    )
  | Ecall (func, el) ->
    (match Hashtbl.find_option typ_fun func with
    | Some (_,t) -> OK t
    | None -> Error (Format.sprintf "function %s not found (unknown type)" func)
    )
  | Eaddrof (e) ->
    let* type_e = type_expr struct_table typ_var typ_fun e in
    OK(Tptr type_e)
  | Eload  e ->
    let* type_e = type_expr struct_table typ_var typ_fun e in
    (match type_e with
    | Tptr t -> OK t
    | _ -> Error "Can't deference a non pointer type")
  | Egetfield (e,s) ->
    let* et = type_expr struct_table typ_var typ_fun e in
    match et with
    | Tptr Tstruct name -> field_type struct_table name s
    | _ -> Error "Can only get field of struct"


let are_types_compatible (t1: typ) (t2:typ): typ res =
  match t1 with
  | Tint
  | Tchar -> (
      match t2 with
      | Tint | Tchar -> OK(t1)
      | _ -> Error (Format.sprintf "Incompatible type %s %s" (string_of_typ t1) (string_of_typ t2))
    )
  | Tptr Tstruct name -> if t2 = Tstruct name || t2 = Tptr (Tstruct name) then OK(t1) else Error (Printf.sprintf "Incompatible type %s %s" (string_of_typ t1) (string_of_typ t2))
  | Tstruct name -> if t2 = Tstruct name || t2 = Tptr (Tstruct name) then OK(t1) else Error (Printf.sprintf "Incompatible type %s %s" (string_of_typ t1) (string_of_typ t2))
  | Tptr t -> (
  match t2 with
  | Tptr t' -> if t = t' then OK(t) else Error "Pointer of two different types"
  | Tint -> OK(Tint)
  | _ -> Error "Incompatible type: pointers are integers"
  )
  | _ -> Error (Format.sprintf "Incompatible type %s %s" (string_of_typ t1) (string_of_typ t2))

let type_expr_binop (op:binop) (t1: typ) (t2:typ): typ res =
    let error = Error (Format.sprintf "Incompatible type %s %s for op" (string_of_typ t1) (string_of_typ t2)) in
    match t1 with
    | Tint
    | Tchar -> (
        match t2 with
        | Tint | Tchar -> OK(t1)
        | Tptr t' -> (
          match op with
          | Eadd -> OK t2
          | _ ->  error
        )
        | _ -> error
      )
    | Tptr t' -> (
      match t2 with
      | Tint | Tchar when op = Eadd || op = Esub -> OK t1
      | Tptr t'' when t' = t'' -> (
        match op with
        | Eclt | Ecle | Ecgt | Ecge | Eceq | Ecne -> OK Tint
        | _ -> error
      )
      | _-> error
    )

    | _ -> error
  
let type_expr_unop (op:unop) (t: typ) : typ res =
  let error = Error (Format.sprintf "Incompatible type %s for unary op" (string_of_typ t)) in
  match t with
  | Tint
  | Tchar -> OK t
  | _ -> error

let rec addr_taken_expr (e: expr) : string Set.t =
  match e with
  | Ebinop (_,e1,e2) -> Set.union (addr_taken_expr e1) (addr_taken_expr e2)
  | Eload (e)
  | Egetfield (e,_)
  | Eunop (_,e) -> addr_taken_expr e
  | Ecall (_,el) -> List.fold_left (fun acc e -> Set.union acc (addr_taken_expr e)) Set.empty el
  | Eaddrof (Evar(v)) -> Set.singleton v
  | Eaddrof _
  | Eint _
  | Echar _
  | Evar _ -> Set.empty

let rec  addr_taken_instr (i: instr) : string Set.t = 
  match i with
  | Iassign (_,e) -> addr_taken_expr e
  | Iif (e,i1,i2) -> Set.union (Set.union (addr_taken_instr i1)(addr_taken_instr i2)) (addr_taken_expr e)
  | Iwhile (e,i) -> Set.union (addr_taken_expr e) (addr_taken_instr i)
  | Iblock (il) -> List.fold_left (fun acc i -> Set.union acc (addr_taken_instr i)) Set.empty il
  | Ireturn e -> addr_taken_expr e
  | Iprint e -> addr_taken_expr e
  | Icall (_,el) -> List.fold_left (fun acc e -> Set.union acc (addr_taken_expr e)) Set.empty el
  | Isetfield(e1,_,e2)
  | Istore (e1,e2) -> Set.union (addr_taken_expr e1)(addr_taken_expr e2)

let gen_funvarinmem i funvartyp struct_table: ((string, int )Hashtbl.t * int) res=
  let var_in_mem = addr_taken_instr i in
  let var_in_mem = Hashtbl.fold (fun name typv acc ->
    match typv with
    | Tstruct _ -> Set.union (Set.singleton name) acc
    | _ -> acc
    ) funvartyp var_in_mem in
  Set.fold (
    fun var acc ->
      let* (table, size) = acc in
      let var_typ = Hashtbl.find funvartyp var in
      let* offset = size_type var_typ struct_table in
      Hashtbl.replace table var size;
      OK (table, size + offset)
    ) var_in_mem (OK (Hashtbl.create (Set.cardinal var_in_mem), 0))

(* [make_eexpr_of_ast a] builds an expression corresponding to a tree [a]. If
   the tree is not well-formed, fails with an [Error] message. *)
let rec make_eexpr_of_ast (a: tree)
  (typ_var : (string,typ) Hashtbl.t)
  (typ_fun : (string, typ list * typ) Hashtbl.t)
  (struct_table: (string, (string * Prog.typ) list) Hashtbl.t)
  : (expr * typ) res =
  let res =
    match a with
    | Node(Tcall, StringLeaf(name)::r) ->
      let args_list = (match r with
      | Node(Targs, l )::[] -> l
      | _ -> []
      ) in
      list_map_res (fun e -> make_eexpr_of_ast e typ_var typ_fun struct_table) args_list >>= fun args_expr ->
        let (args_expr, args_t) = List.split args_expr in
        let returned_expression = Ecall(name, args_expr) in
        let (args_typ,_) = Hashtbl.find typ_fun name in
        if (List.length args_typ) != (List.length args_t) then Error (Format.sprintf "%s called with %d instead of %d" name  (List.length args_t)  (List.length args_typ) ) else
        let does_args_typ_match = List.for_all2 (
          fun t1 t2 ->
            match are_types_compatible t1 t2 with
            | OK _ -> true
            | Error _ -> false
        ) args_typ args_t in
        if does_args_typ_match then 
        let* t = type_expr struct_table typ_var typ_fun returned_expression in
        OK (returned_expression, t)
        else Error (Format.sprintf "arguments types does not match in expression calling %s" name)

    | Node(t, [e1; e2]) when tag_is_binop t ->
          make_eexpr_of_ast e1 typ_var typ_fun struct_table >>= fun (expr1,t1) ->
          make_eexpr_of_ast e2 typ_var typ_fun struct_table >>= fun (expr2,t2) ->
          let op = binop_of_tag t in
          let* t1 = type_expr_binop op t1 t2 in
          OK (Ebinop(op, expr1 , expr2),t1)
    | Node(t, [e1]) when tag_is_unop t -> make_eexpr_of_ast e1 typ_var typ_fun struct_table >>= fun (expr1,t1) ->
          let op = unop_of_tag t in
          let* t1 = type_expr_unop op t1 in
          OK(Eunop(op,expr1), t1)
    | Node(t,[e1]) when t = Tint -> make_eexpr_of_ast e1 typ_var typ_fun struct_table
    | IntLeaf(num) -> OK(Eint(num), Tint)
    | StringLeaf(str) ->
      let returned_expression = Evar(str) in
      let* t = type_expr struct_table typ_var typ_fun returned_expression in
      OK(returned_expression, t)
    | CharLeaf(c) -> OK(Echar(c),Tchar)
    | Node(Tp, [e1]) ->
      let* (expr, t) = make_eexpr_of_ast e1 typ_var typ_fun struct_table in
      (match t with
      | Tptr t ->  OK(Eload expr, t)
      | _ -> Error (Printf.sprintf "Can't Load non pointer here %s" (string_of_ast a)))
    | Node(Taddrof, [e1]) ->
      let* (expr, t) = make_eexpr_of_ast e1 typ_var typ_fun struct_table in 
      OK(Eaddrof(expr), Tptr t)
    | Node(Tstructunfoll,[e1; StringLeaf field]) ->
      let* (struct_expr, t) = make_eexpr_of_ast e1 typ_var typ_fun struct_table in
      (match t with
      | Tptr Tstruct s-> 
        let* t = field_type struct_table s field in
        OK(Egetfield(struct_expr, field), t)
      | _ -> Error "Bad expression"
      )
    | Node(Tstructpoint,[e1; StringLeaf field]) -> (
      let* (struct_expr, t) = make_eexpr_of_ast e1 typ_var typ_fun struct_table in
      match t with
      | Tptr Tstruct name ->
        let* t' = field_type struct_table name field in
        OK (Eaddrof (Egetfield (struct_expr,field)), Tptr t')
      | Tstruct name ->
        let* t' = field_type struct_table name field in
        OK (Eaddrof (Egetfield ((Eaddrof struct_expr),field)), Tptr t')
      | _ -> Error "It should point to struct"
    )
    | _ -> Error (Printf.sprintf "Unacceptable ast in make_eexpr_of_ast %s"
                    (string_of_ast a))
  in
  (match res with
    OK o -> res
  | Error msg -> Error (Format.sprintf "In make_eexpr_of_ast %s:\n%s"
                          (string_of_ast a) msg))


let rec make_einstr_of_ast (a: tree) typ_var typ_fun struct_table (ret_typ:typ): instr res =
  let res =
    match a with
    | NullLeaf -> OK(Iblock [])
    | Node(Tcall, StringLeaf(name)::r) ->
      let args_list = (match r with
      | Node(Targs, l )::[] -> l
      | _ -> []
      ) in
      list_map_res (fun e -> make_eexpr_of_ast e typ_var typ_fun struct_table) args_list >>= fun args_expr ->
        let (args_expr, args_t) = List.split args_expr in
        let returned_expression = Icall(name, args_expr) in
        let (args_typ,_) = Hashtbl.find typ_fun name in
        if (List.length args_typ) != (List.length args_t) then Error (Format.sprintf "%s called with %d instead of %d" name  (List.length args_t)  (List.length args_typ) ) else
        let does_args_typ_match = List.for_all2 (
          fun t1 t2 ->
            match are_types_compatible t1 t2 with
            | OK _ -> true
            | Error _ -> false
          ) args_typ args_t in
        if does_args_typ_match then 
        let* t = type_expr struct_table typ_var typ_fun (Ecall(name, args_expr)) in
        OK (returned_expression)
        else Error (Format.sprintf "arguments types does not match in expression calling %s" name)

    | Node(t, e1::i1::i2) when t = Tif ->
      make_eexpr_of_ast e1 typ_var typ_fun struct_table>>= fun (expr1,_) ->
      make_einstr_of_ast i1 typ_var typ_fun struct_table ret_typ>>= fun inst1 ->
      (match i2 with
      | [] -> OK (Iif(expr1,inst1,Iblock([])))
      | [i2'] -> make_einstr_of_ast (i2') typ_var typ_fun struct_table ret_typ >>= fun inst2 -> OK (Iif(expr1, inst1 , inst2))
      | _ ->  Error (Printf.sprintf "Unacceptable ast in make_einstr_of_ast %s" (string_of_ast a)))

    | Node(t,[i1]) when t = Tassign -> make_einstr_of_ast i1 typ_var typ_fun struct_table ret_typ

    | Node(Tassignvar,[StringLeaf(str);e1;ty]) ->
      let* ty = is_type ty in
      if ty = Tvoid then Error (Format.sprintf "variable %s cannot be void" str) else
      (Hashtbl.replace typ_var str ty;
      (match e1 with
      | NullLeaf -> OK(Iblock [])
      | _ -> make_eexpr_of_ast e1 typ_var typ_fun struct_table >>= fun (expr1,ty') ->
        if ty = ty'
          then OK(Iassign(str,expr1))
          else Error (Format.sprintf "Can't assign %s to %s" (string_of_typ ty') str) 
      ))

    | Node(Tassignvar,[varTree;e1]) ->
      (match varTree with
      | StringLeaf str -> (match Hashtbl.find_option typ_var str with
        | Some ty -> make_eexpr_of_ast e1 typ_var typ_fun struct_table >>= fun (expr1,ty') ->
          if ty = ty'
            then OK(Iassign(str,expr1))
            else Error (Format.sprintf "Can't assign %s to %s" (string_of_typ ty') str) 
        | None -> Error (Format.sprintf "Can't assign untyped var %s" str))
      | Node(Tp,_) ->
        let* (affected_expr, t1) = make_eexpr_of_ast varTree typ_var typ_fun struct_table in
        let* (affectation, t2) = make_eexpr_of_ast e1 typ_var typ_fun struct_table in
        let* _ = are_types_compatible t1 t2 in
        (match affected_expr with 
        | Eload e ->  OK(Istore(e, affectation))
        | _ -> Error "This error should not be triggered, I just unpile a useless expression")
      | Node(Tstructpoint,[st; StringLeaf field]) ->
        let* (affected_expr, t1) = make_eexpr_of_ast st typ_var typ_fun struct_table in
        (match t1 with
        | Tptr Tstruct name ->
          let* t1 = field_type struct_table name field in
          let* (affectation, t2) = make_eexpr_of_ast e1 typ_var typ_fun struct_table in
          let* _ = are_types_compatible t1 t2 in
          OK(Isetfield(affected_expr,field,affectation))
        | Tstruct name -> Error (Printf.sprintf "NAnan: %s" (name))
        | _ -> Error (Printf.sprintf "can't unfol non struct: %s" (string_of_typ t1))
        )
        
        
      | _ -> Error (Printf.sprintf "Unacceptable ast in make_einstr_of_ast %s" (string_of_ast a))
      )

    | Node(t,[e1;i1]) when t = Twhile ->
      make_eexpr_of_ast e1 typ_var typ_fun struct_table>>= fun (expr1,_) ->
      make_einstr_of_ast i1 typ_var typ_fun struct_table ret_typ >>= fun inst1 ->
      OK(Iwhile(expr1, inst1))

    | Node(t,li) when t = Tblock ->
      list_map_res (fun i -> make_einstr_of_ast i typ_var typ_fun struct_table ret_typ) li >>= fun inst_res ->
      OK(Iblock(inst_res))

    | Node(t,[e1]) when t = Treturn -> 
      make_eexpr_of_ast e1 typ_var typ_fun struct_table>>= fun (expr1,ty) ->
        (match are_types_compatible ret_typ ty with
        | OK(t) ->  OK(Ireturn expr1)
        | Error q-> Error (Format.sprintf "Bad return type %s vs %s" (string_of_typ ret_typ) (string_of_typ ty)))

    | _ -> Error (Printf.sprintf "Unacceptable ast in make_einstr_of_ast %s" (string_of_ast a))
  in
  match res with
    OK o -> res
  | Error msg -> Error (Format.sprintf "In make_einstr_of_ast %s:\n%s"
                          (string_of_ast a) msg)

let make_ident (a: tree) : (string * typ) res =
  match a with
  | Node (Targ, [t;s]) ->
    let* t = is_type t in
    (match t with
    | Tstruct n -> OK(string_of_stringleaf s, Tptr t)
    | _ -> OK (string_of_stringleaf s, t))
  | a -> Error (Printf.sprintf "make_ident: unexpected AST: %s"
                  (string_of_ast a))

let make_fundef_of_ast (a: tree) (typ_fun: (string, typ list * typ) Hashtbl.t) struct_table: (string * efun) option res =
  match a with
  | Node (Tfundef, [retTyp; StringLeaf fname; Node (Tfunargs, fargs); fbody]) ->
    is_type retTyp >>= fun retTyp ->
    list_map_res make_ident fargs >>= fun fargs ->
    let typ_var = Hashtbl.of_list fargs in
    Hashtbl.replace typ_fun fname ((snd (List.split fargs)), retTyp);
    (match fbody with
    | NullLeaf -> OK(None)
    | _ -> make_einstr_of_ast fbody typ_var typ_fun struct_table retTyp>>= fun fbodye ->
      let* (funvarinmem, funstksz) = gen_funvarinmem fbodye typ_var struct_table in
      OK(Some (fname, {
        funargs = fargs;
        funbody = fbodye;
        funvartyp = typ_var;
        funrettype = retTyp;
        funvarinmem = funvarinmem;
        funstksz = funstksz;
        })))
  | _ ->
    Error (Printf.sprintf "make_fundef_of_ast: Expected a Tfundef, got %s."
             (string_of_ast a))

let make_struct_def_of_ast (a:tree): (string* (string * Prog.typ) list) res =
  match a with
  | Node(Tstructdef,[StringLeaf name; Node(Tstructcontent, el)]) ->(
    let* fields = list_map_res (
      fun e ->
        match e with
        | Node(Tstructelement, [type_tree; StringLeaf field]) ->
          let* field_type = is_type type_tree in
          OK(field, field_type)
        | _ -> Error (Printf.sprintf "AST: bad Tstructelement for %s" name)
        ) el in
    OK (name,fields)
  )
  | _ -> Error "AST Tstrucdef malformed"
  
let make_eprog_of_ast (a: tree) : eprog res =
  match a with
  | Node (Tlistglobdef, l) ->
    let fun_typ = Hashtbl.create (List.length l) in
    Hashtbl.replace fun_typ "print" ([Tint], Tvoid);
    Hashtbl.replace fun_typ "print_int" ([Tint], Tvoid);
    Hashtbl.replace fun_typ "print_char" ([Tchar], Tvoid);
    List.fold_left (fun acc a ->
      let* (funl, structt) = acc in
      match a with
      | Node(Tfundef,_) -> (
        let* efun_option = make_fundef_of_ast a fun_typ structt in
          match efun_option with
          | Some  (fname, efun) -> OK ((fname, Gfun efun)::funl, structt)
          | _ -> acc
      )

      | Node(Tstructdef,_) -> (
        let* (name, structdef) = make_struct_def_of_ast a in
        Hashtbl.replace structt name structdef;
        OK(funl, structt)
      )

      | _ -> Error (Printf.sprintf "Unexpected tag at root: %s" (string_of_ast a))
      ) (OK ([],Hashtbl.create 0)) l
  | _ ->
    Error (Printf.sprintf "make_fundef_of_ast: Expected a Tlistglobdef, got %s."
             (string_of_ast a))

let pass_elang ast : eprog res=
  match make_eprog_of_ast ast with
  | Error msg ->
    record_compile_result ~error:(Some msg) "Elang";
    Error msg
  | OK  ep ->
    dump !e_dump dump_e (fst ep) (fun file () ->
        add_to_report "e" "E" (Code (file_contents file))); OK ep

