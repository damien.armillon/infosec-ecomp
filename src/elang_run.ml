open Elang
open Batteries
open BatList
open Prog
open Utils
open Builtins
open Utils
open Elang_gen

let binop_bool_to_int f x y = if f x y then 1 else 0

(* [eval_binop b x y] évalue l'opération binaire [b] sur les arguments [x]
   et [y]. *)
let eval_binop (b: binop) : int -> int -> int =
  match b with
   | Eadd -> fun x y -> x + y
   | Esub -> fun x y -> x - y
   | Emul -> fun x y -> x * y
   | Ediv -> fun x y -> x / y
   | Emod -> fun x y -> (x mod y)
   | Exor -> fun x y -> x lxor y
   | Eclt -> fun x y -> binop_bool_to_int (<) x y
   | Ecle -> fun x y -> binop_bool_to_int (<=) x y
   | Ecgt -> fun x y -> binop_bool_to_int (>) x y
   | Ecge -> fun x y -> binop_bool_to_int (>=) x y
   | Eceq -> fun x y -> binop_bool_to_int (=) x y
   | Ecne -> fun x y -> binop_bool_to_int (!=) x y

(* [eval_unop u x] évalue l'opération unaire [u] sur l'argument [x]. *)
let eval_unop (u: unop) : int -> int =
  match u with
   | Eneg -> fun x -> -x

(* [eval_eexpr fun_typ oc ep st e] évalue l'expression [e] dans l'état [st]. Renvoie une
   erreur si besoin. *)
let rec eval_eexpr fun_typ oc (ep: eprog) st (sp: int) func_env (e : expr) : (int * int Prog.state) res =
   let (fun_list, struct_table) = ep in
   match e with
   | Ebinop(op,expr1,expr2) ->
      let* t1 = type_expr struct_table func_env.funvartyp fun_typ expr1 in
      let* t2 = type_expr struct_table func_env.funvartyp fun_typ expr2 in
      let* (value1, st) = eval_eexpr fun_typ oc ep st sp func_env expr1 in
      let* (value2, st) = eval_eexpr fun_typ oc ep st sp func_env expr2 in
      (match t1 with
      | Tptr t ->
         let* size = size_type t struct_table in
         OK((eval_binop op value1 (value2 * size)), st)
      | _ -> 
         (match t2 with
         | Tptr t ->
            let* size = size_type t struct_table in
            OK((eval_binop op (value1 * size) value2), st)
         | _ -> 
             OK((eval_binop op value1 value2), st)
         ))

   | Eunop(op,expr) ->
      let* (value,st) = eval_eexpr fun_typ oc ep st sp func_env expr in
      OK((eval_unop op value), st)

   | Evar(str) ->
      (match Hashtbl.find_option st.env str with 
      | None -> (
         match Hashtbl.find_option func_env.funvarinmem str with
         | Some offset ->
            let* mem_to_read = size_type (Hashtbl.find func_env.funvartyp str) struct_table in
            let* value = Mem.read_bytes_as_int st.mem offset (mem_to_read) in
            OK(value,st)
         | None -> Error (Format.sprintf "Unknown variable %s\n" str))
      | Some(value) -> OK(value, st))
   | Eint(n) -> OK(n, st)
   | Echar c -> OK(Char.code c,st)
   | Ecall(name,expr_list) ->
      let* (args, st) = List.fold_left (fun acc expr ->
         let* (expr_l, st) = acc in
         let* (arg_expr, st) = eval_eexpr fun_typ oc ep st sp func_env expr in
         OK(expr_l@[arg_expr], st)
         ) (OK([], st)) expr_list in
      (match find_function fun_list name with
      | OK f ->
         let* (res,st) = eval_efun fun_typ oc ep st (sp+ f.funstksz) f name args in
         (match res with
         | Some(n) -> OK(n, st)
         | None -> Error (Format.sprintf "Error with function %s" name))
      | Error _ ->
         let* ret = do_builtin oc st.mem name args in
         (match ret with
         | Some n -> OK(n, st)
         | None -> Error (Format.sprintf "Error: %s doesn't return anything" name)))
      | Eload e ->
         let* (addr,st) = eval_eexpr fun_typ oc ep st sp func_env e in
         let* type_to_load = type_expr struct_table func_env.funvartyp fun_typ e in
         (match type_to_load with
         | Tptr t ->
            let* size = size_type t struct_table in
            let* value = Mem.read_bytes_as_int st.mem addr size in
            OK(value, st)
         | _ -> Error "Can't load non pointer")
      | Eaddrof e ->
         (match e with
         | Evar str ->
            let offset = Hashtbl.find func_env.funvarinmem str in
            OK(offset + sp,st)
         | _ -> Error "& can only be applied on var")
      | Egetfield (e,s) -> Error "Egetfield not here yet"
      

(* [eval_einstr struct_table fun_typ oc st ins] évalue l'instrution [ins] en partant de l'état [st].

   Le paramètre [oc] est un "output channel", dans lequel la fonction "print"
   écrit sa sortie, au moyen de l'instruction [Format.fprintf].

   Cette fonction renvoie [(ret, st')] :

   - [ret] est de type [int option]. [Some v] doit être renvoyé lorsqu'une
   instruction [return] est évaluée. [None] signifie qu'aucun [return] n'a eu
   lieu et que l'exécution doit continuer.

   - [st'] est l'état mis à jour. *)
and eval_einstr fun_typ oc (ep: eprog) (st: int state) (sp:int) func_env (ins: instr) :
  (int option * int state) res =
  let (fun_list, struct_table) = ep in
  let rec eval_list_instrs st li : (int option * int state) res =
   match li with 
   | [] -> OK(None,st)
   | instr::next_instrs -> eval_einstr fun_typ oc ep st sp func_env instr >>= fun result ->
       match result with
       | (None, st) -> eval_list_instrs st next_instrs
       | (Some(n), st) -> OK(result)
 in
 let rec eval_while st e i: (int option * int state) res =
   let* (condition,st) = eval_eexpr fun_typ oc ep st sp func_env e in
      if condition = 1 then 
         eval_einstr fun_typ oc ep st sp func_env i >>= fun result_loop ->
         match result_loop with
         | (None, st)-> (eval_while st e i)
         | (Some(n), st) -> OK(result_loop)
      else OK(None, st)
  in 
   match ins with
   | Iassign(str, expr) ->
      let* (value, st) = eval_eexpr fun_typ oc ep st sp func_env expr in
      (match Hashtbl.find_option func_env.funvarinmem str with
      | Some offset ->
         let type_var = Hashtbl.find func_env.funvartyp str in
         let* size_write = size_type type_var struct_table in
         let bytes_to_write = split_bytes (size_write) value in
         let* value = Mem.write_bytes st.mem offset bytes_to_write in
         OK(None,st)

      | None ->
         Hashtbl.replace st.env str value;
         OK(None,st))

   | Iif(expr,instr1,instr2) ->
      let* (condition,st) = eval_eexpr fun_typ oc ep st sp func_env expr in
        if condition=1 then eval_einstr fun_typ oc ep st sp func_env instr1 else eval_einstr fun_typ oc ep st sp func_env instr2
   
   | Iwhile(expr,instr) -> eval_while st expr instr

   | Iblock(instrs) -> eval_list_instrs st instrs

   | Ireturn(expr) ->
      let* (result,st) = eval_eexpr fun_typ oc ep st sp func_env expr
      in OK(Some(result), st)

   | Iprint(expr) ->
      let* (result, st) = eval_eexpr fun_typ oc ep st sp func_env expr in
      Format.fprintf oc "%d\n" result; OK(None, st)
   
   | Icall(name,expr_list) ->
      let* (args, st) = List.fold_left (fun acc expr ->
         let* (expr_l, st) = acc in
         let* (arg_expr, st) = eval_eexpr fun_typ oc ep st sp func_env expr in
         OK(expr_l@[arg_expr], st)
         ) (OK([], st)) expr_list in
      (match find_function fun_list name with
      | OK f ->
         let* (_,st) = eval_efun fun_typ oc ep st (sp + f.funstksz) f name args in
         OK (None,st)
      | Error _ ->
         let* ret = do_builtin oc st.mem name args in
         OK (ret, st))
   | Istore(e1, e2) ->
      let* (addr,st) = eval_eexpr fun_typ oc ep st sp func_env e1 in
      let* (value,st) = eval_eexpr fun_typ oc ep st sp func_env e2 in
      let* t = type_expr struct_table func_env.funvartyp fun_typ e1 in
      let* size_write = size_type t struct_table in
      let bytes_to_write = split_bytes (size_write) value in
      let* _ = Mem.write_bytes st.mem addr bytes_to_write in
      OK(None,st)

   | Isetfield(e1,str,e2) -> (
      let* (addr,st) = eval_eexpr fun_typ oc ep st sp func_env e1 in
      let* (value,st) = eval_eexpr fun_typ oc ep st sp func_env e2 in
      let* t = type_expr struct_table func_env.funvartyp fun_typ e1 in
      match t with
      | Tptr (Tstruct name) -> (
         let* offset = field_offset struct_table name str in
         let* affect_type = field_type struct_table name str in
         let* size_write = size_type affect_type struct_table in
         let bytes_to_write = split_bytes (size_write) value in
         let* _ = Mem.write_bytes st.mem addr bytes_to_write in
         OK(None, st)
         )
      | _ -> Error (Printf.sprintf "%s is not a struct. can't do Isetfield" (string_of_typ t))
   )

(* [eval_efun fun_typ oc st f fname vargs] évalue la fonction [f] (dont le nom est
   [fname]) en partant de l'état [st], avec les arguments [vargs].

   Cette fonction renvoie un couple (ret, st') avec la même signification que
   pour [eval_einstr struct_table fun_typ]. *)
and eval_efun fun_typ oc (ep: eprog) (st: int state) (sp: int) (f: efun)
    (fname: string) (vargs: int list)
  : (int option * int state) res =
  (* L'environnement d'une fonction (mapping des variables locales vers leurs
     valeurs) est local et un appel de fonction ne devrait pas modifier les
     variables de l'appelant. Donc, on sauvegarde l'environnement de l'appelant
     dans [env_save], on appelle la fonction dans un environnement propre (Avec
     seulement ses arguments), puis on restore l'environnement de l'appelant. *)
  let { funargs; funbody; funstksz; funvarinmem; funrettype} = f in
  let env_save = Hashtbl.copy st.env in
  let env = Hashtbl.create 17 in
  match List.iter2 (fun a v -> Hashtbl.replace env a v) (fst (List.split funargs)) vargs with
  | () ->
    eval_einstr fun_typ oc ep { st with env } sp f funbody >>= fun (v, st') ->
    OK (v, { st' with env = env_save })
  | exception Invalid_argument _ ->
    Error (Format.sprintf
             "E: Called function %s with %d arguments, expected %d.\n"
             fname (List.length vargs) (List.length funargs)
          )

(* [eval_eprog oc ep memsize params] évalue un programme complet [ep], avec les
   arguments [params].

   Le paramètre [memsize] donne la taille de la mémoire dont ce programme va
   disposer. Ce n'est pas utile tout de suite (nos programmes n'utilisent pas de
   mémoire), mais ça le sera lorsqu'on ajoutera de l'allocation dynamique dans
   nos programmes.

   Renvoie:

   - [OK (Some v)] lorsque l'évaluation de la fonction a lieu sans problèmes et renvoie une valeur [v].

   - [OK None] lorsque l'évaluation de la fonction termine sans renvoyer de valeur.

   - [Error msg] lorsqu'une erreur survient.
   *)
let eval_eprog oc (ep: eprog) (memsize: int) (params: int list)
  : int option res =
  let (fun_list, struct_table) = ep in
  let st = init_state memsize in
  let fun_typ = Hashtbl.create (3+(List.length fun_list)) in
  List.iter (fun (name,pro) ->
   match pro with
   | Gfun f -> Hashtbl.replace fun_typ name ((snd (List.split f.funargs)),f.funrettype);
   )fun_list;
  Hashtbl.replace fun_typ "print" ([Tint], Tvoid);
  Hashtbl.replace fun_typ "print_int" ([Tint], Tvoid);
  Hashtbl.replace fun_typ "print_char" ([Tchar], Tvoid);
  find_function fun_list "main" >>= fun f ->
  (* ne garde que le nombre nécessaire de paramètres pour la fonction "main". *)
  let n = List.length f.funargs in
  let params = take n params in
  let { env } = st in
  let { funargs } = f in
  List.iter2 (fun argname argval -> Hashtbl.replace env argname argval) (fst (List.split funargs)) params;
  eval_efun fun_typ oc ep st 0 f "main" params >>= fun (v, st) ->
  OK v
