tokens SYM_EOF SYM_IDENTIFIER<string> SYM_INTEGER<int> SYM_PLUS SYM_MINUS SYM_ASTERISK SYM_DIV SYM_MOD
tokens SYM_LPARENTHESIS SYM_RPARENTHESIS SYM_LBRACE SYM_RBRACE
tokens SYM_ASSIGN SYM_SEMICOLON SYM_RETURN SYM_IF SYM_WHILE SYM_ELSE SYM_COMMA
tokens SYM_EQUALITY SYM_NOTEQ SYM_LT SYM_LEQ SYM_GT SYM_GEQ
tokens SYM_VOID SYM_CHAR SYM_INT SYM_CHARACTER<char> SYM_AMPERSAND
tokens SYM_STRUCT SYM_POINT
non-terminals S INSTR INSTRS LINSTRS ELSE EXPR FACTOR
non-terminals LPARAMS REST_PARAMS
non-terminals IDENTIFIER INTEGER
non-terminals FUNDEF FUNDEFS
non-terminals ADD_EXPRS ADD_EXPR
non-terminals MUL_EXPRS MUL_EXPR
non-terminals CMP_EXPRS CMP_EXPR
non-terminals EQ_EXPRS EQ_EXPR
non-terminals BLOC
non-terminals IN_INSTR L_CALL_PARAMS REST_CALL_PARAMS FACTORS FACTOR_IDENTIFIER
non-terminals SYM_TYPE ASSIGN_EXPR FUNBODY
non-terminals VAR TYPE_PT
non-terminals STRUCTCONTENT STRUCTDEF DEFS STRUCTORFUN SYM_TYPE_NO_STRUCT DOTVAR
axiom S
{

  open Symbols
  open Ast
  open BatPrintf
  open BatBuffer
  open Batteries
  open Utils

  let resolve_associativity term other =
    List.fold_left (fun acc (tag, tree) -> Node(tag,acc::tree)) term other

}

rules
S -> DEFS SYM_EOF {  Node (Tlistglobdef, $1) }
DEFS -> FUNDEF DEFS {Node(Tfundef, $1) :: $2}
DEFS -> STRUCTDEF DEFS {$1 :: $2}
DEFS -> {[]}
STRUCTDEF -> SYM_STRUCT SYM_IDENTIFIER STRUCTORFUN {$3 $2}
STRUCTORFUN -> SYM_LBRACE STRUCTCONTENT SYM_RBRACE SYM_SEMICOLON {fun name -> Node(Tstructdef, [StringLeaf(name);Node(Tstructcontent,$2)])}
STRUCTORFUN -> SYM_IDENTIFIER SYM_LPARENTHESIS LPARAMS SYM_RPARENTHESIS FUNBODY {fun name -> Node(Tfundef,[Node(Tstruct,[StringLeaf(name)]);StringLeaf($1);Node(Tfunargs,$3);$5])}
STRUCTCONTENT -> SYM_TYPE SYM_IDENTIFIER SYM_SEMICOLON STRUCTCONTENT {Node(Tstructelement,[$1; StringLeaf($2)])::$4}
STRUCTCONTENT -> {[]}
BLOC -> SYM_LBRACE INSTRS SYM_RBRACE {Node(Tblock, $2)}
FUNDEF -> SYM_TYPE_NO_STRUCT SYM_IDENTIFIER SYM_LPARENTHESIS LPARAMS SYM_RPARENTHESIS FUNBODY { [$1; StringLeaf($2); (Node(Tfunargs, $4)); $6] }
FUNBODY -> BLOC {$1}
FUNBODY -> SYM_SEMICOLON {NullLeaf}
LPARAMS -> {[]}
LPARAMS -> SYM_TYPE SYM_IDENTIFIER REST_PARAMS {(Node(Targ, [$1; (StringLeaf $2)])) :: $3}
REST_PARAMS -> SYM_COMMA SYM_TYPE SYM_IDENTIFIER REST_PARAMS {(Node(Targ, [$2; (StringLeaf $3)])):: $4}
REST_PARAMS -> {[]}
L_CALL_PARAMS -> {[]}
L_CALL_PARAMS -> EXPR REST_CALL_PARAMS {[Node(Targs, $1::$2)]}
REST_CALL_PARAMS -> {[]}
REST_CALL_PARAMS -> SYM_COMMA EXPR REST_CALL_PARAMS {$2::$3}
IN_INSTR -> SYM_LPARENTHESIS L_CALL_PARAMS SYM_RPARENTHESIS SYM_SEMICOLON {fun x -> Node(Tcall, x::$2)}
IN_INSTR -> SYM_ASSIGN EXPR SYM_SEMICOLON {fun x -> Node(Tassign, [Node(Tassignvar,[x; $2])])}
ASSIGN_EXPR -> SYM_ASSIGN EXPR {[$2]}
ASSIGN_EXPR -> {[NullLeaf]}
INSTRS -> {[]}
INSTRS -> INSTR INSTRS {$1::$2}
INSTR -> BLOC {$1}
INSTR -> VAR IN_INSTR {$2 ($1 Tstructpoint )}
INSTR -> SYM_TYPE SYM_IDENTIFIER ASSIGN_EXPR SYM_SEMICOLON {Node(Tassign, [Node(Tassignvar , (StringLeaf $2)::$3@[$1])])}
INSTR -> SYM_IF SYM_LPARENTHESIS EXPR SYM_RPARENTHESIS BLOC ELSE {Node(Tif, $3::$5::$6)}
INSTR -> SYM_WHILE SYM_LPARENTHESIS EXPR SYM_RPARENTHESIS BLOC {Node(Twhile, [$3;$5])}
INSTR -> SYM_RETURN EXPR SYM_SEMICOLON {Node(Treturn, [$2])}
ELSE -> SYM_ELSE BLOC {[$2]}
ELSE -> {[]}
EXPR -> EQ_EXPR EQ_EXPRS {resolve_associativity $1 $2}
EQ_EXPRS -> SYM_EQUALITY EQ_EXPR {[(Tceq, [$2])]}
EQ_EXPRS -> SYM_NOTEQ EQ_EXPR {[(Tne, [$2])]}
EQ_EXPRS -> {[]}
EQ_EXPR -> CMP_EXPR CMP_EXPRS {resolve_associativity $1 $2}
CMP_EXPRS -> SYM_LT CMP_EXPR {[(Tclt, [$2])]}
CMP_EXPRS -> SYM_GT CMP_EXPR {[(Tcgt, [$2])]}
CMP_EXPRS -> SYM_LEQ CMP_EXPR {[(Tcle, [$2])]}
CMP_EXPRS -> SYM_GEQ CMP_EXPR {[(Tcge, [$2])]}
CMP_EXPRS -> {[]}
CMP_EXPR -> ADD_EXPR ADD_EXPRS {resolve_associativity $1 $2}
ADD_EXPRS -> SYM_PLUS ADD_EXPR ADD_EXPRS {(Tadd, [$2])::$3}
ADD_EXPRS -> SYM_MINUS ADD_EXPR ADD_EXPRS {(Tsub, [$2])::$3}
ADD_EXPRS -> {[]}
ADD_EXPR -> MUL_EXPR MUL_EXPRS {resolve_associativity $1 $2}
MUL_EXPRS -> SYM_ASTERISK MUL_EXPR MUL_EXPRS {(Tmul, [$2])::$3}
MUL_EXPRS -> SYM_DIV MUL_EXPR MUL_EXPRS {(Tdiv, [$2])::$3}
MUL_EXPRS -> SYM_MOD MUL_EXPR MUL_EXPRS {(Tmod, [$2])::$3}
MUL_EXPRS -> {[]}
MUL_EXPR -> FACTORS { $1 }
FACTORS -> FACTOR {$1}
FACTORS -> SYM_MINUS FACTOR {Node(Tneg,[$2])}
FACTOR_IDENTIFIER -> {fun x -> x}
FACTOR_IDENTIFIER -> SYM_LPARENTHESIS L_CALL_PARAMS SYM_RPARENTHESIS {fun x -> Node(Tcall, x::$2)}
FACTOR -> SYM_INTEGER {IntLeaf($1)}
FACTOR -> SYM_CHARACTER {CharLeaf($1)}
FACTOR -> VAR FACTOR_IDENTIFIER {$2 ($1 Tstructunfoll)}
FACTOR -> SYM_LPARENTHESIS EXPR SYM_RPARENTHESIS {$2}
SYM_TYPE -> SYM_STRUCT TYPE_PT SYM_IDENTIFIER {$2 (Node(Tstruct,[StringLeaf($3)]))}
SYM_TYPE -> SYM_TYPE_NO_STRUCT {$1}
SYM_TYPE_NO_STRUCT -> SYM_INT TYPE_PT {$2 (Node(Tint,[]))}
SYM_TYPE_NO_STRUCT -> SYM_CHAR TYPE_PT { $2 (Node(Tchar,[]))}
SYM_TYPE_NO_STRUCT -> SYM_VOID TYPE_PT {$2 (Node(Tvoid,[]))}
TYPE_PT -> SYM_ASTERISK TYPE_PT {fun x -> $2 (Node(Tptr,[x]))}
TYPE_PT -> {fun x -> x}
VAR -> SYM_IDENTIFIER DOTVAR { fun x ->
  let a = $2 x in
  match a with
  | [] -> StringLeaf($1)
  | _ -> resolve_associativity (StringLeaf($1)) a
  }
VAR -> SYM_ASTERISK VAR {fun x -> Node(Tp,[$2 x])}
VAR -> SYM_AMPERSAND VAR {fun x -> Node(Taddrof,[$2 x])}
DOTVAR -> SYM_POINT SYM_IDENTIFIER DOTVAR {fun x ->
  let res = $3 x in
  match res with
  | [] -> (x,[StringLeaf $2])::[]
  | _ -> (Tstructpoint,[StringLeaf $2])::res
  }
DOTVAR -> {fun x -> []}
