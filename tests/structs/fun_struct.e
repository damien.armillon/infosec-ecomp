struct point {
  int x;
  int y;
};

struct point f(int x, int y) {
  struct point p;
  p.x = x;
  p.y = y;
  return p;
}

int main() {
  struct point p;
  p = f(1,2);
  return p.y;
}