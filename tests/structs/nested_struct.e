struct point {
  int x;
  int y;
};

struct segment {
  struct point b;
  struct point a;
};

int main() {
  struct segment ab;
  struct point b;
  b.x= 4;
  b.y = 5;
  ab.b = b;
  ab.a.x = 1;
  ab.a.y = 2;
  return ab.b.y;
}